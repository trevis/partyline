﻿using Decal.Adapter;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;

namespace PartyLine.Lib {
    public class Settings : INotifyPropertyChanged {

        private bool _enabled;
        private string _loginToken = "";
        private ulong _channel = 0;
        private string _webhookURL = "";

        public bool Enabled {
            get { return _enabled; }
            set { _enabled = value; NotifyPropertyChanged(); }
        }

        public string LoginToken {
            get { return _loginToken; }
            set { _loginToken = value.Trim(); NotifyPropertyChanged(); }
        }

        public ulong Channel {
            get { return _channel; }
            set { _channel = value; NotifyPropertyChanged(); }
        }

        public string WebhookURL {
            get { return _webhookURL; }
            set { _webhookURL = value.Trim(); NotifyPropertyChanged(); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static string SettingsPath {
            get {
                var server = CoreManager.Current.CharacterFilter.Server;
                var character = CoreManager.Current.CharacterFilter.Name;

                return Path.Combine(PluginCore.PluginStorageDirectory, $"{server}.{character}.xml");
            }
        }

        public static Settings LoadSettings() {
            try {
                if (File.Exists(SettingsPath)) {
                    XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                    using (FileStream fs = new FileStream(SettingsPath, FileMode.Open)) {
                        var settings = (Settings)serializer.Deserialize(fs);
                        return settings;
                    }
                }
            }
            catch (Exception ex) { Logger.LogException(ex); }

            return new Settings();
        }

        public Settings() {
            PropertyChanged += Settings_PropertyChanged;
        }

        ~Settings() {
            PropertyChanged -= Settings_PropertyChanged;
        }

        private void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            Save();
        }

        private void Save() {
            try {
                if (!Directory.Exists(Path.GetDirectoryName(SettingsPath))) {
                    Directory.CreateDirectory(Path.GetDirectoryName(SettingsPath));
                }

                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                using (TextWriter writer = new StreamWriter(SettingsPath)) {
                    serializer.Serialize(writer, this);
                    writer.Close();
                }
            }
            catch {}
        }
    }
}
