﻿using Discord;
using Discord.WebSocket;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PartyLine.Lib {
    class DiscordBot : IDisposable {
        private DiscordSocketClient discord;

        public bool IsRunning { get; private set; }

        public event EventHandler Changed;

        public DiscordBot() {
            PluginCore.Settings.PropertyChanged += Settings_PropertyChanged;
            Start();
        }

        private void Settings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if (PluginCore.Settings.Enabled != IsRunning) {
                if (PluginCore.Settings.Enabled)
                    Start();
                else
                    Stop();
            }
        }

        public async void Start() {
            if (IsRunning || !PluginCore.Settings.Enabled)
                return;

            var config = new DiscordSocketConfig() {
                GatewayIntents = GatewayIntents.DirectMessages | GatewayIntents.GuildMessages | GatewayIntents.Guilds
            };

            discord = new DiscordSocketClient(config);

            discord.Log += Log;
            discord.MessageReceived += Discord_MessageReceived;
            await discord.LoginAsync(TokenType.Bot, PluginCore.Settings.LoginToken);
            await discord.StartAsync();

            IsRunning = true;
            Changed?.Invoke(this, EventArgs.Empty);
        }

        private string FormatSocketMessage(SocketMessage arg) {
            var message = arg.ToString();

            // handle tags
            foreach (var tag in arg.Tags) {
                var tagMarkup = "";
                var tagContents = "";

                switch (tag.Type) {
                    case TagType.ChannelMention:
                        tagMarkup = $"<#{tag.Key}>";
                        tagContents = $"#{tag.Value}";
                        break;
                    case TagType.RoleMention:
                        tagMarkup = $"<@&{tag.Key}>";
                        tagContents = $"@{tag.Value}(Role)";
                        break;
                    case TagType.UserMention:
                        tagMarkup = $"<@!{tag.Key}>";
                        tagContents = $"@{GetUserDisplayName(tag.Value as SocketGuildUser)}";
                        break;
                    case TagType.Emoji:
                        tagMarkup = $"<#{tag.Key}>";
                        tagContents = tag.Value.ToString() + "(" + tag.Value.GetType() + ")";
                        break;
                    case TagType.EveryoneMention:
                        //tagMarkup = $"<@?{tag.Key}>";
                        //tagContents = "@everyone";
                        break;
                    case TagType.HereMention:
                        //tagMarkup = $"<@?{tag.Key}>";
                        //tagContents = "@here";
                        break;
                }

                // [PL] Found tag: trevis#8444 (UserMention) // 375369745941135365 // UserMention // trevis#8444
                //Logger.WriteToChat($"Found tag: {tag} // {tag.Index} // {tag.Key} // {tag.Type} // {tag.Value}");

                message = message.Replace(tagMarkup, tagContents);
            }
            return message;
        }

        private string GetUserDisplayName(SocketUser user) {
            var author = (user as SocketGuildUser).Nickname;
            if (string.IsNullOrWhiteSpace(author))
                author = user.ToString().Split('#')[0];

            return author;
        }

        private async Task Discord_MessageReceived(SocketMessage arg) {
            try {
                // skip bots and messages not from the channel we are hooked up to
                if (arg.Author.IsBot || arg.Channel.Id != PluginCore.Settings.Channel)
                    return;

                var author = Regex.Replace(GetUserDisplayName(arg.Author), @"[^\u0000-\u007F]+", string.Empty);
                var message = Regex.Replace(FormatSocketMessage(arg), @"[^\u0000-\u007F]+", string.Empty);

                // TODO: handle emojis and @tags
                if (!string.IsNullOrWhiteSpace(message)) {
                    Logger.WriteToChat($"Forwarding message from discord: {author} says, {message}");
                    PluginCore.DispatchChatToBoxWithPluginIntercept($"/a {author} says, {message}");
                }
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        private Task Log(LogMessage msg) {
            Logger.Log(msg.ToString());
            return Task.CompletedTask;
        }

        public void Stop() {
            if (!IsRunning)
                return;

            discord.Dispose();
            IsRunning = false;
            Changed?.Invoke(this, EventArgs.Empty);
        }

        public void Dispose() {
            Stop();
            PluginCore.Settings.PropertyChanged -= Settings_PropertyChanged;
        }
    }
}
