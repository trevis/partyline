﻿using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Discord.Webhook;
using MyClasses.MetaViewWrappers;
using Newtonsoft.Json.Linq;
using PartyLine.Lib;

namespace PartyLine {
    [WireUpBaseEvents]
    [MVView("PartyLine.PluginView.xml")]
    [MVWireUpControlEvents]
    [FriendlyName("PartyLine")]
    public class PluginCore : PluginBase {
        internal static string PluginStorageDirectory { get; set; } = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"Decal Plugins\PartyLine");
        internal static Settings Settings { get; private set; }
        internal DiscordBot DiscordBot { get; private set; }
        internal DiscordWebhookClient WebhookClient { get; private set; }

        private Regex ChatMessageCapture = new Regex(@"^\[(?<channel>\S+)\] <Tell:IIDString:\d+:[^>]+>(?<character>[^<]+)<\\Tell> says, ""(?<message>.*)""$", RegexOptions.Compiled); 

        #region Startup/Shutdown
        protected override void Startup() {
            try {
                ServicePointManager.SecurityProtocol = SecurityProtocolTypeExtensions.Tls12;

                MVWireupHelper.WireupStart(this, Host);
                UIChannel.TooltipText = "This is the id of the discord channel you want to hook up to.";
                UILoginToken.TooltipText = "This is the discord bot login token.";
                UIWebhookURL.TooltipText = "The webhook url for your bot to post messages back to discord.";
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        protected override void Shutdown() {
            try {
                Core.ChatBoxMessage -= Core_ChatBoxMessage;

                if (DiscordBot != null) {
                    DiscordBot.Changed -= DiscordBot_Changed;
                    DiscordBot.Dispose();
                }
                if (Settings != null)
                    Settings.PropertyChanged -= Settings_PropertyChanged;
                MVWireupHelper.WireupEnd(this);
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }
        #endregion

        #region Decal Wrappers
        [DllImport("Decal.dll")]
        static extern int DispatchOnChatCommand(ref IntPtr str, [MarshalAs(UnmanagedType.U4)] int target);

        public static bool Decal_DispatchOnChatCommand(string cmd) {
            IntPtr bstr = Marshal.StringToBSTR(cmd);

            try {
                bool eaten = (DispatchOnChatCommand(ref bstr, 1) & 0x1) > 0;

                return eaten;
            }
            finally {
                Marshal.FreeBSTR(bstr);
            }
        }

        public static void DispatchChatToBoxWithPluginIntercept(string cmd) {
            try {
                if (!Decal_DispatchOnChatCommand(cmd))
                    CoreManager.Current.Actions.InvokeChatParser(cmd);
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }
        #endregion

        #region Event Handlers
        [BaseEvent("LoginComplete", "CharacterFilter")]
        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                Settings = Settings.LoadSettings();
                Settings.PropertyChanged += Settings_PropertyChanged;
                Init();
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        private void Init() {
            try {
                if (DiscordBot != null)
                    DiscordBot.Dispose();
                if (WebhookClient != null)
                    WebhookClient.Dispose();

                if (Settings.Enabled) {
                    DiscordBot = new DiscordBot();
                    WebhookClient = new DiscordWebhookClient(Settings.WebhookURL);
                    UpdateUI();
                    if (!hasDecalEventsSubscribed) {
                        Core.ChatBoxMessage += Core_ChatBoxMessage;
                        DiscordBot.Changed += DiscordBot_Changed;
                        hasDecalEventsSubscribed = true;
                    }
                }
                else {
                    Core.ChatBoxMessage -= Core_ChatBoxMessage;
                    DiscordBot.Changed -= DiscordBot_Changed;
                    hasDecalEventsSubscribed = false;
                }
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        private void DiscordBot_Changed(object sender, EventArgs e) {
            try {
                UpdateUI();
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        private void Core_ChatBoxMessage(object sender, ChatTextInterceptEventArgs e) {
            try {
                if (Settings.Enabled && ChatMessageCapture.IsMatch(e.Text)) {
                    var match = ChatMessageCapture.Match(e.Text);
                    var channel = match.Groups["channel"].Value;
                    var character = match.Groups["character"].Value;
                    var message = match.Groups["message"].Value;

                    if (character.Equals(Core.CharacterFilter.Name))
                        return;

                    if (channel.ToLower().Equals("allegiance")) {
                        try {
                            WebhookClient.SendMessageAsync(message, false, null, character);
                        }
                        catch (Exception ex) { Logger.LogException(ex); }
                    }
                }
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        private void Settings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            try {
                UpdateUI();
                if (e.PropertyName == "Enabled")
                    Init();
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }
        #endregion

        #region UI
        [MVControlReference("UILoginToken")]
        private ITextBox UILoginToken = null;

        [MVControlReference("UIChannel")]
        private ITextBox UIChannel = null;

        [MVControlReference("UIWebhookURL")]
        private ITextBox UIWebhookURL = null;

        [MVControlReference("UIEnabled")]
        private ICheckBox UIEnabled = null;

        [MVControlReference("UIStatus")]
        private IStaticText UIStatus = null;
        private bool hasDecalEventsSubscribed;

        private void UpdateUI() {
            UILoginToken.Text = Settings.LoginToken;
            UIChannel.Text = Settings.Channel.ToString();
            UIWebhookURL.Text = Settings.WebhookURL;
            UIEnabled.Checked = Settings.Enabled;
            UIStatus.Text = DiscordBot.IsRunning ? "Running" : "Stopped";
        }

        [MVControlEvent("UIEnabled", "Change")]
        void UIEnabled_Change(object sender, MVCheckBoxChangeEventArgs e) {
            try {
                Logger.WriteToChat("Settings.Enabled = " + UIEnabled.Checked.ToString());
                Settings.Enabled = UIEnabled.Checked;
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        [MVControlEvent("UILoginToken", "Change")]
        void UILoginToken_Change(object sender, MVTextBoxChangeEventArgs e) {
            try {
                Logger.WriteToChat("Settings.LoginToken: " + UILoginToken.Text.ToString());
                Settings.LoginToken = UILoginToken.Text;
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        [MVControlEvent("UIChannel", "Change")]
        void UIChannel_Change(object sender, MVTextBoxChangeEventArgs e) {
            try {
                if (ulong.TryParse(UIChannel.Text, out ulong parsedChannel)) {
                    Settings.Channel = parsedChannel;
                    Logger.WriteToChat("Settings.Channel: " + Settings.Channel.ToString());
                }
                else {
                    Logger.WriteToChat($"Invalid Channel: {UIChannel.Text}");
                }
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        [MVControlEvent("UIWebhookURL", "Change")]
        void UIWebhookURL_Change(object sender, MVTextBoxChangeEventArgs e) {
            try {
                Logger.WriteToChat("Settings.WebhookURL: " + UIWebhookURL.Text.ToString());
                Settings.WebhookURL = UIWebhookURL.Text;
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }
        #endregion
    }
}
